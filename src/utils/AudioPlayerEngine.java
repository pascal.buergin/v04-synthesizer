package utils;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

public class AudioPlayerEngine {

	final static int FREQ = 44100;
	final SourceDataLine speakers;
	final AudioFormat format = new AudioFormat(FREQ, 16, 1, true, true);

	short[] clear = new short[FREQ];
	short[] buffer = new short[FREQ];

	public AudioPlayerEngine() throws LineUnavailableException {
		DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class, format);
		speakers = (SourceDataLine) AudioSystem.getLine(dataLineInfo);
		speakers.open(format);
		speakers.start();

		playerThread = new Thread(playerRunnable, "Audio Player Thread");
		playerThread.start();
	}

	private final ArrayBlockingQueue<Integer> signalQueue = new ArrayBlockingQueue<>(1);

	final static Object[] sampleLock = new Object[] {};
	final AtomicBoolean stop = new AtomicBoolean(false);

	private Thread playerThread;
	private final Runnable playerRunnable = new Runnable() {

		private short[] currentBuffer = new short[FREQ];
		ByteBuffer byteBuffer = ByteBuffer.allocate(currentBuffer.length * 2);

		@Override
		public void run() {
			for (;;) {
				try {
					for (;;) {
						Thread.interrupted();
						int len;

						try {
							len = signalQueue.take();
						} catch (InterruptedException e) {
							continue;
						}

						stop.set(false);

						synchronized (sampleLock) {
							System.arraycopy(buffer, 0, currentBuffer, 0, len);
						}

						for (int i = 0; i < len; i++) {
							System.out.println(currentBuffer[i]);
						}

						byteBuffer.position(0);
						byteBuffer.asShortBuffer().put(currentBuffer);

						for (;;) {

							if (speakers.available() > 0) {
								speakers.write(byteBuffer.array(), 0, len * 2);
								System.out.println("available " + speakers.available());
							} else {
								try {
									Thread.sleep(10);
								} catch (Exception e) {

								}
							}

							if (stop.get()) {
								break;
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				try {
					System.out.println("restarting in 1s");
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	};

	public void play(double[] samples) throws IOException {
		if (samples.length > buffer.length) {
			throw new IOException(String.format("max allowed %d samples (each 4byte)", buffer.length));
		}

		synchronized (sampleLock) {
			System.arraycopy(this.clear, 0, this.buffer, 0, this.buffer.length);
			for (int i = 0; i < samples.length; i++) {
				this.buffer[i] = (short) (samples[i] * Short.MAX_VALUE);
			}
		}

		speakers.flush();
		stop.set(true);
		playerThread.interrupt();
		signalQueue.offer(samples.length);
	}

	public void stop() {
		signalQueue.clear();
		stop.set(true);
		playerThread.interrupt();
		speakers.flush();
	}
}
